﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace GameOfLive
{
  public partial class ToolBar : Form
  {
    Spielfeld spielfeld = new Spielfeld();

    public ToolBar()
    {
      InitializeComponent();
      spielfeld.GenerationHasChanged += OnGenerationChanged;
    }

    private void OnGenerationChanged(object sender, EventArgs e)
    {
        SetzeGeneration(); 
    }
      
    private void SetzeGeneration()
    {
      if (InvokeRequired)
      {
          Invoke(new Action(SetzeGeneration));
      }
      else
      {
        lbAnzahlGenerationen.Text = spielfeld.AktuelleGeneration.ToString();
      }
    }

    //Überschriebene OnLoad-Methode um Spielfeld anzuzeigen
    protected override void OnLoad(EventArgs e)
    {
      spielfeld.Show();
      base.OnLoad(e);
    }

    //Berechnet die nächste generation
    private void btnGenBerechnen_Click(object sender, EventArgs e)
    {
      spielfeld.GenBerechnen();
    }

    //Berechnet die nächsten generationen bist Stop gedrückt wird.
    void btnMehrereGenBerechnen_Click(object sender, EventArgs e)
    {
      btnEineGeneration.Enabled = false;
      btnMehrereGenerationen.Enabled = false;
      Thread td1 = new Thread(new ThreadStart(spielfeld.GenBerechnenbisStop));
      td1.Start();
    }

    //Beendet das Generationberechnen von btnMehrereGenBerechnen_Click.
    private void btnMehrereGenBerechnenStop_Click(object sender, EventArgs e)
    {
      spielfeld.GenBerechnenStop();
      btnEineGeneration.Enabled = true;
      btnMehrereGenerationen.Enabled = true;
    }

    //-------------------------------------------------------------------------------------
    private void AnzahlZeilenChanged(object sender, EventArgs e)
    {
      spielfeld.AnzahlZeilenChanged((int)numericUpDownAnzahlZeilen.Value);
    }

    private void AnzahlSpaltenChanged(object sender, EventArgs e)
    {
      spielfeld.AnzahlSpaltenChanced((int)numericUpDownAnzahlSpalten.Value);
    }

    //-------------------------------------------------------------------------------------
    private void GeschwindigkeitChanged(object sender, EventArgs e)
    {
      switch (NummericUpDowngeeschwindigkeitsStufe.Value)
      {
        case 1:
          spielfeld.geschwindigkeitAendern(1000);
          break;
        case 2:
          spielfeld.geschwindigkeitAendern(500);
          break;
        case 3:
          spielfeld.geschwindigkeitAendern(200);
          break;
        case 4:
          spielfeld.geschwindigkeitAendern(100);
          break;
        case 5:
          spielfeld.geschwindigkeitAendern(20);
          break;
        case 6:
          spielfeld.geschwindigkeitAendern(0);
          break;
      }
    }

    //---------------------------------------------------------------------------------------------------

    private void btnSpielfeldClear_Click(object sender, EventArgs e)
    {
      spielfeld.clear();
    }

    private void btnSerialisieren_Click(object sender, EventArgs e)
    {
      spielfeld.Serialisieren();
    }

    private void btnDeserialisieren_Click(object sender, EventArgs e)
    {
      spielfeld.Deserialisieren();
    }

    private void btnSpielfelderneutÖffnen_Click(object sender, EventArgs e)
    {
      if (!spielfeld.Visible)
      {
        spielfeld = new Spielfeld();
        spielfeld.Show();
        spielfeld.GenerationHasChanged += OnGenerationChanged;
      }
    }

    private void ToolBar_FormClosing(object sender, FormClosingEventArgs e)
    {
      if(spielfeld.BerechnungLaeuft)
      {
        e.Cancel = true;
      }
    }

    private void ToolBar_Load(object sender, EventArgs e)
    {
      this.ClientSize = new System.Drawing.Size(225, btnSpielfelderneutÖffnen.Bottom + 30);
    }
  }
}
