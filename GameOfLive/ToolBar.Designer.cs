﻿namespace GameOfLive
{
  partial class ToolBar
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToolBar));
      this.btnEineGeneration = new System.Windows.Forms.Button();
      this.btnMehrereGenerationen = new System.Windows.Forms.Button();
      this.btnMehrereGenerationenStop = new System.Windows.Forms.Button();
      this.numericUpDownAnzahlZeilen = new System.Windows.Forms.NumericUpDown();
      this.NummericUpDowngeeschwindigkeitsStufe = new System.Windows.Forms.NumericUpDown();
      this.btnSpielfeldClear = new System.Windows.Forms.Button();
      this.lbAnzahlGenerationen = new System.Windows.Forms.Label();
      this.lblBeschriftung1 = new System.Windows.Forms.Label();
      this.lblBeschriftung2 = new System.Windows.Forms.Label();
      this.lblBeschriftung3 = new System.Windows.Forms.Label();
      this.lblBeschreibung4 = new System.Windows.Forms.Label();
      this.numericUpDownAnzahlSpalten = new System.Windows.Forms.NumericUpDown();
      this.btnSerialisieren = new System.Windows.Forms.Button();
      this.btnDeserialisieren = new System.Windows.Forms.Button();
      this.btnSpielfelderneutÖffnen = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAnzahlZeilen)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.NummericUpDowngeeschwindigkeitsStufe)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAnzahlSpalten)).BeginInit();
      this.SuspendLayout();
      // 
      // btnEineGeneration
      // 
      resources.ApplyResources(this.btnEineGeneration, "btnEineGeneration");
      this.btnEineGeneration.Name = "btnEineGeneration";
      this.btnEineGeneration.UseVisualStyleBackColor = true;
      this.btnEineGeneration.Click += new System.EventHandler(this.btnGenBerechnen_Click);
      // 
      // btnMehrereGenerationen
      // 
      resources.ApplyResources(this.btnMehrereGenerationen, "btnMehrereGenerationen");
      this.btnMehrereGenerationen.Name = "btnMehrereGenerationen";
      this.btnMehrereGenerationen.UseVisualStyleBackColor = true;
      this.btnMehrereGenerationen.Click += new System.EventHandler(this.btnMehrereGenBerechnen_Click);
      // 
      // btnMehrereGenerationenStop
      // 
      resources.ApplyResources(this.btnMehrereGenerationenStop, "btnMehrereGenerationenStop");
      this.btnMehrereGenerationenStop.Name = "btnMehrereGenerationenStop";
      this.btnMehrereGenerationenStop.UseVisualStyleBackColor = true;
      this.btnMehrereGenerationenStop.Click += new System.EventHandler(this.btnMehrereGenBerechnenStop_Click);
      // 
      // numericUpDownAnzahlZeilen
      // 
      resources.ApplyResources(this.numericUpDownAnzahlZeilen, "numericUpDownAnzahlZeilen");
      this.numericUpDownAnzahlZeilen.Maximum = new decimal(new int[] {
            350,
            0,
            0,
            0});
      this.numericUpDownAnzahlZeilen.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.numericUpDownAnzahlZeilen.Name = "numericUpDownAnzahlZeilen";
      this.numericUpDownAnzahlZeilen.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.numericUpDownAnzahlZeilen.ValueChanged += new System.EventHandler(this.AnzahlZeilenChanged);
      // 
      // NummericUpDowngeeschwindigkeitsStufe
      // 
      resources.ApplyResources(this.NummericUpDowngeeschwindigkeitsStufe, "NummericUpDowngeeschwindigkeitsStufe");
      this.NummericUpDowngeeschwindigkeitsStufe.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
      this.NummericUpDowngeeschwindigkeitsStufe.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.NummericUpDowngeeschwindigkeitsStufe.Name = "NummericUpDowngeeschwindigkeitsStufe";
      this.NummericUpDowngeeschwindigkeitsStufe.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
      this.NummericUpDowngeeschwindigkeitsStufe.ValueChanged += new System.EventHandler(this.GeschwindigkeitChanged);
      // 
      // btnSpielfeldClear
      // 
      resources.ApplyResources(this.btnSpielfeldClear, "btnSpielfeldClear");
      this.btnSpielfeldClear.Name = "btnSpielfeldClear";
      this.btnSpielfeldClear.UseVisualStyleBackColor = true;
      this.btnSpielfeldClear.Click += new System.EventHandler(this.btnSpielfeldClear_Click);
      // 
      // lbAnzahlGenerationen
      // 
      resources.ApplyResources(this.lbAnzahlGenerationen, "lbAnzahlGenerationen");
      this.lbAnzahlGenerationen.Name = "lbAnzahlGenerationen";
      // 
      // lblBeschriftung1
      // 
      resources.ApplyResources(this.lblBeschriftung1, "lblBeschriftung1");
      this.lblBeschriftung1.Name = "lblBeschriftung1";
      // 
      // lblBeschriftung2
      // 
      resources.ApplyResources(this.lblBeschriftung2, "lblBeschriftung2");
      this.lblBeschriftung2.Name = "lblBeschriftung2";
      // 
      // lblBeschriftung3
      // 
      resources.ApplyResources(this.lblBeschriftung3, "lblBeschriftung3");
      this.lblBeschriftung3.Name = "lblBeschriftung3";
      // 
      // lblBeschreibung4
      // 
      resources.ApplyResources(this.lblBeschreibung4, "lblBeschreibung4");
      this.lblBeschreibung4.Name = "lblBeschreibung4";
      // 
      // numericUpDownAnzahlSpalten
      // 
      resources.ApplyResources(this.numericUpDownAnzahlSpalten, "numericUpDownAnzahlSpalten");
      this.numericUpDownAnzahlSpalten.Maximum = new decimal(new int[] {
            350,
            0,
            0,
            0});
      this.numericUpDownAnzahlSpalten.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.numericUpDownAnzahlSpalten.Name = "numericUpDownAnzahlSpalten";
      this.numericUpDownAnzahlSpalten.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.numericUpDownAnzahlSpalten.ValueChanged += new System.EventHandler(this.AnzahlSpaltenChanged);
      // 
      // btnSerialisieren
      // 
      resources.ApplyResources(this.btnSerialisieren, "btnSerialisieren");
      this.btnSerialisieren.Name = "btnSerialisieren";
      this.btnSerialisieren.UseVisualStyleBackColor = true;
      this.btnSerialisieren.Click += new System.EventHandler(this.btnSerialisieren_Click);
      // 
      // btnDeserialisieren
      // 
      resources.ApplyResources(this.btnDeserialisieren, "btnDeserialisieren");
      this.btnDeserialisieren.Name = "btnDeserialisieren";
      this.btnDeserialisieren.UseVisualStyleBackColor = true;
      this.btnDeserialisieren.Click += new System.EventHandler(this.btnDeserialisieren_Click);
      // 
      // btnSpielfelderneutÖffnen
      // 
      resources.ApplyResources(this.btnSpielfelderneutÖffnen, "btnSpielfelderneutÖffnen");
      this.btnSpielfelderneutÖffnen.Name = "btnSpielfelderneutÖffnen";
      this.btnSpielfelderneutÖffnen.UseVisualStyleBackColor = true;
      this.btnSpielfelderneutÖffnen.Click += new System.EventHandler(this.btnSpielfelderneutÖffnen_Click);
      // 
      // ToolBar
      // 
      resources.ApplyResources(this, "$this");
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnSpielfelderneutÖffnen);
      this.Controls.Add(this.btnDeserialisieren);
      this.Controls.Add(this.btnSerialisieren);
      this.Controls.Add(this.lblBeschreibung4);
      this.Controls.Add(this.numericUpDownAnzahlSpalten);
      this.Controls.Add(this.lblBeschriftung3);
      this.Controls.Add(this.lblBeschriftung2);
      this.Controls.Add(this.lblBeschriftung1);
      this.Controls.Add(this.lbAnzahlGenerationen);
      this.Controls.Add(this.btnSpielfeldClear);
      this.Controls.Add(this.NummericUpDowngeeschwindigkeitsStufe);
      this.Controls.Add(this.numericUpDownAnzahlZeilen);
      this.Controls.Add(this.btnMehrereGenerationenStop);
      this.Controls.Add(this.btnMehrereGenerationen);
      this.Controls.Add(this.btnEineGeneration);
      this.MaximizeBox = false;
      this.Name = "ToolBar";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ToolBar_FormClosing);
      this.Load += new System.EventHandler(this.ToolBar_Load);
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAnzahlZeilen)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.NummericUpDowngeeschwindigkeitsStufe)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAnzahlSpalten)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnEineGeneration;
    private System.Windows.Forms.Button btnMehrereGenerationen;
    public System.Windows.Forms.Button btnMehrereGenerationenStop;
    private System.Windows.Forms.NumericUpDown numericUpDownAnzahlZeilen;
    private System.Windows.Forms.NumericUpDown NummericUpDowngeeschwindigkeitsStufe;
    public System.Windows.Forms.Button btnSpielfeldClear;
    private System.Windows.Forms.Label lbAnzahlGenerationen;
    private System.Windows.Forms.Label lblBeschriftung1;
    private System.Windows.Forms.Label lblBeschriftung2;
    private System.Windows.Forms.Label lblBeschriftung3;
    private System.Windows.Forms.Label lblBeschreibung4;
    private System.Windows.Forms.NumericUpDown numericUpDownAnzahlSpalten;
    public System.Windows.Forms.Button btnSerialisieren;
    public System.Windows.Forms.Button btnDeserialisieren;
    public System.Windows.Forms.Button btnSpielfelderneutÖffnen;
  }
}