﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace GameOfLive
{
  public partial class Spielfeld : Form
  {
    public event EventHandler GenerationHasChanged;

    bool mRun;
    int mGeschwindigkeit = 100;
    Feld[,] mSpielstand;
    int mAnzahlSpalten = 10;
    int mAnzahlZeilen = 10;
    int mOffsetSpalte = 1;
    int mOffsetZeile = 1;
    bool mFlagXgesetzt = false;
    Feld[][] mSpeicherungFeld;

    public Spielfeld()
    {
      InitializeComponent();
    }

    private int mGeneration;
    public int AktuelleGeneration
    {
      get { return mGeneration; }
      set
      {
        mGeneration = value;
        GenerationHasChanged?.Invoke(this, EventArgs.Empty);
      }
    }
    public bool BerechnungLaeuft => mRun;


    //Überschriebene OnLoad-methode_________________________________________________________________________________________________________________
    protected override void OnLoad(EventArgs Event)
    {
      mSpielstand = neues2dFeld();

      Thread countThread = null;
      bool stop = false;
      int indexX = ((Cursor.Position.X - Location.X) - 6) / mOffsetSpalte;
      int indexY = ((Cursor.Position.Y - this.Location.Y) - 30) / mOffsetZeile;
      int indexXvorher = indexX;
      int indexYvorher = indexY;

      MouseDown += (s, e) =>
      {
        stop = false;
        countThread = new Thread(() =>
        {
          while (!stop)
          {
            if (indexXvorher != indexX || indexYvorher != indexY)
            {
              ZelleGecklicked(this, EventArgs.Empty);
              indexXvorher = indexX;
              indexYvorher = indexY;
            }
            indexX = ((Cursor.Position.X - Location.X) - 6) / mOffsetSpalte;
            indexY = ((Cursor.Position.Y - this.Location.Y) - 30) / mOffsetZeile;
          }
        });
        countThread.Start();
      };

      MouseUp += (s, e) =>
      {
        stop = true;
      };

      base.OnLoad(Event);
    }
    //OnPaint-Methode_______________________________________________________________________________________________________________________________
    protected override void OnPaint(PaintEventArgs e)
    {
      Graphics mG = e.Graphics;
      mG.Clear(Color.Black);

      for (int Spalten = 0; Spalten < mAnzahlSpalten; Spalten++)
      {
        for (int Zeilen = 0; Zeilen <mAnzahlZeilen; Zeilen++)
        {
          if (mSpielstand[Spalten, Zeilen].Lebend)
          {
            if (mSpielstand[Spalten, Zeilen].GenerationenLebend < 255)
            {
              Brush myColor = new SolidBrush(Color.FromArgb(255, 255 - (mSpielstand[Spalten, Zeilen].GenerationenLebend), 255 - (mSpielstand[Spalten, Zeilen].GenerationenLebend)));
              mG.FillRectangle(myColor, Spalten * mOffsetSpalte, Zeilen * mOffsetZeile, mOffsetSpalte, mOffsetZeile);
            }
            else
            {
              Brush myColor = new SolidBrush(Color.FromArgb(255, 0, 0));
              mG.FillRectangle(myColor, Spalten * mOffsetSpalte, Zeilen * mOffsetZeile, mOffsetSpalte, mOffsetZeile);
            }
          }
        }
      }
    }

    //Ändert den Zustand eine Feld wenn es Angeklickt wird_________________________________________________________________________________________________________________________________________
    private void ZelleGecklicked(object sender, EventArgs e)
    {
      int indexX = ((Cursor.Position.X - Location.X) - 6) / mOffsetSpalte;
      int indexY = ((Cursor.Position.Y - this.Location.Y) - 30) / mOffsetZeile;
      if (mFlagXgesetzt)
      {
        FuegeGeladeneDatenEin(indexX, indexY);
        mFlagXgesetzt = false;
        Invalidate();
      }
      else
      {
        AendereZustandEinesFeldes(indexX, indexY);
      }
    }

    private void FuegeGeladeneDatenEin(int aIndexX, int aIndexY)
    {


      for (int i = 0; i < mSpeicherungFeld.Length; i++)
      {
        for (int j = 0; j < mSpeicherungFeld[0].Length; j++)
          mSpielstand[i+aIndexX, j+aIndexY] = mSpeicherungFeld[i][j];
      } 
    }

    private void AendereZustandEinesFeldes(int aX, int aY)
    {
      if(aX >= 0 & aY >= 0)
      {
        if ((aX < mAnzahlSpalten) & (aY < mAnzahlZeilen))
        {
          mSpielstand[aX, aY].Lebend = !mSpielstand[aX, aY].Lebend;
          Invalidate();
          AktuelleGeneration = 0;
        }
      }
    }


    //Offset ändern falls grösse des Spielfeldes geändert wurde________________________________________________________________________________________________________
    private void Form1_ResizeEnd(object sender, EventArgs e)
    {
      mOffsetSpalte = Size.Width / mAnzahlSpalten;
      mOffsetZeile = Size.Height / mAnzahlZeilen;
      Size AngepassteGrösse = new Size();
      AngepassteGrösse.Width = mAnzahlSpalten * mOffsetSpalte;
      AngepassteGrösse.Height = mAnzahlZeilen * mOffsetZeile;
      Invalidate();
      ClientSize = AngepassteGrösse;
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------

    // Wechseln der Anzahl Felder auf Zeile oder Spalte

    //Anzahl in der Spalte ändern___________________________________________________________________________________________________________________________________
    public void AnzahlSpaltenChanced(int aI)
    {
      mAnzahlSpalten = aI;
      mSpielstand = neues2dFeld();
      mOffsetSpalte = (Size.Width) / mAnzahlSpalten;
      Invalidate();
    }

    //Anzahl in der Zeile ändern___________________________________________________________________________________________________________________________________
    public void AnzahlZeilenChanged(int aI)
    {
      mAnzahlZeilen = aI;
      mSpielstand= neues2dFeld();
      mOffsetZeile = (Size.Height) / mAnzahlZeilen;
      Invalidate();
    }

    //Spielfeld clearen_____________________________________________________________________________________________________________________
    internal void clear()
    {
      mSpielstand = neues2dFeld();
      Invalidate();
      AktuelleGeneration = 0;
    }


    //----------------------------------------------------------------------------------------------------------------------------------------------------------------

    //nächste Generation berechnen____________________________________________________________________________________________________________________________
    public void GenBerechnen()
    {
      NexteGenerationBerechnen(); ;
      AktuelleGeneration++;
      Invalidate();
    }
    //dauernd generationen Berechnen bis Stop gedrückt wird____________________________________________________________________________________________________________________________
    public void GenBerechnenbisStop()
    {
      mRun = true;
      while (mRun == true)
      {
        NexteGenerationBerechnen(); ;
        Invalidate();
        Thread.Sleep(mGeschwindigkeit);
        AktuelleGeneration++;
      }
    }

    // Stopbefehl von GenBerechnenbisStop________________________________________________________________________________________________________________
    public void GenBerechnenStop()
    {
      mRun = false;
    }

    // Die Geschwindigkeit von GenBerechnenbisStop ändern___________________________________________________________________________________________________
    public void geschwindigkeitAendern(int aI)
    {
      mGeschwindigkeit = aI;
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //berechnet die nächste Generation__________________________________________________________________________________________________________________
    private void NexteGenerationBerechnen()
    {
      Feld[,] ZwischengespeicherteSpielstand = Clone(mSpielstand);

      for (int Spalte = 0; Spalte < mAnzahlSpalten; Spalte++)
      {
        for (int Zeile = 0; Zeile < mAnzahlZeilen; Zeile++)
        {
          int anzahlNachbaren = AnzahlnachbarenBerechnen(Spalte, Zeile);

          if (mSpielstand[Spalte, Zeile].Lebend)
          {
            if (anzahlNachbaren == 2)
            {
              ZwischengespeicherteSpielstand[Spalte, Zeile].GenerationenLebend++;
            }
            else if (anzahlNachbaren == 3)
            {
              ZwischengespeicherteSpielstand[Spalte, Zeile].GenerationenLebend++;
            }
            else
            {
              ZwischengespeicherteSpielstand[Spalte, Zeile].Lebend = false;
            }
          }
          else
          {
            if (anzahlNachbaren == 3)
            {
              ZwischengespeicherteSpielstand[Spalte, Zeile].Lebend = true;
              ZwischengespeicherteSpielstand[Spalte, Zeile].GenerationenLebend = 0;
            }
          }
        }
      }
      mSpielstand = ZwischengespeicherteSpielstand;
    }

    //Berechnet die Anzahl lebender Nachbaren eines Feldes______________________________________________________________________________________________________________________________________________ 

    private int AnzahlnachbarenBerechnen(int aSpalten, int aZeilen)
    {
      int anzahlNachbaren = 0;

      if (aZeilen > 0)
      {
        if (aSpalten > 0)
        {
          if (mSpielstand[aSpalten - 1, aZeilen - 1].Lebend)
          {
            anzahlNachbaren++;
          }
        }
        if (mSpielstand[aSpalten, aZeilen - 1].Lebend)
        {
          anzahlNachbaren++;
        }
        if (aSpalten + 1 < mAnzahlSpalten)
        {
          if (mSpielstand[aSpalten + 1, aZeilen - 1].Lebend)

            anzahlNachbaren++;
        }
      }

      if (aSpalten > 0)
      {
        if (mSpielstand[aSpalten - 1, aZeilen].Lebend)
        {
          anzahlNachbaren++;
        }
      }

      if (aSpalten + 1 < mAnzahlSpalten)
      {
        if (mSpielstand[aSpalten + 1, aZeilen].Lebend)
        {
          anzahlNachbaren++;
        }
      }

      if (aZeilen + 1 < mAnzahlZeilen)
      {
        if (aSpalten > 0)
        {
          if (mSpielstand[aSpalten - 1, aZeilen + 1].Lebend)
          {
            anzahlNachbaren++;
          }
        }

        if (mSpielstand[aSpalten, aZeilen + 1].Lebend)
        {
          anzahlNachbaren++;
        }

        if (aSpalten + 1 < mAnzahlSpalten)
        {
          if (mSpielstand[aSpalten + 1, aZeilen + 1].Lebend)
          {
            anzahlNachbaren++;
          }
        }
      }

      return anzahlNachbaren;
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // Serialisieren

    public void Serialisieren()
    {
      SaveFileDialog dialog = new SaveFileDialog();
      dialog.Filter = "xmlDateien (*.xml)|*.xml";
      dialog.ShowDialog();

      string pfad = dialog.FileName;

      Feld[][] SpeicherungFeld = new Feld[mAnzahlSpalten][];
      for (int i = 0; i < mSpielstand.GetLength(0); i++)
      {
        SpeicherungFeld[i] = new Feld[mSpielstand.GetLength(1)];

        for (int j = 0; j < mSpielstand.GetLength(1); j++)
          SpeicherungFeld[i][j] = mSpielstand[i, j];
      }
      if (pfad != string.Empty)
      {
        if (File.Exists(pfad)) { File.Delete(pfad); }
        XmlSerializer Writer = new XmlSerializer(typeof(Feld[][]));
        using (FileStream file = File.OpenWrite(pfad))
        {
          Writer.Serialize(file, SpeicherungFeld);
        }
      }
    }

    // Deserialisieren

    public void Deserialisieren()
    {
      OpenFileDialog dialog = new OpenFileDialog();
      dialog.Filter = "xmlDateien (*.xml)|*.xml";
      dialog.ShowDialog();

      string pfad = dialog.FileName;

     XmlSerializer Reader = new XmlSerializer(typeof(Feld[][]));
      if (pfad != string.Empty)
      {
        using (FileStream file = File.OpenRead(pfad))
        {
          mSpeicherungFeld = (Feld[][])Reader.Deserialize(file);
        }

        if (mSpeicherungFeld.Length >= mAnzahlSpalten | mSpeicherungFeld[0].Length >= mAnzahlZeilen)
        {
          mSpielstand = new Feld[mSpeicherungFeld.Length, mSpeicherungFeld[0].Length];

          FuegeGeladeneDatenEin(0, 0);

           mAnzahlSpalten = mSpeicherungFeld.Length;
          mAnzahlZeilen = mSpeicherungFeld[0].Length;
          Form1_ResizeEnd(this, EventArgs.Empty);
        }
        
        else
        {
          mFlagXgesetzt = true;
        }
      }
      Invalidate();
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
      //Clone funktion um ein 2d feld zu Duplizieren_________________________________________________________________________________________________________________
    private Feld[,] Clone(Feld[,] aSpielfeld)
    {
      Feld[,] ErstellterKlon = neues2dFeld();

      for (int Spalte = 0; Spalte < mAnzahlSpalten; Spalte++)
      {
        for (int Zeile = 0; Zeile < mAnzahlZeilen; Zeile++)
        {
          ErstellterKlon[Spalte, Zeile].Lebend = aSpielfeld[Spalte, Zeile].Lebend;
          ErstellterKlon[Spalte, Zeile].GenerationenLebend = aSpielfeld[Spalte, Zeile].GenerationenLebend;
        }
      }
      return ErstellterKlon;
    }

    //Erstellt ein neues 2d Feld____________________________________________________________________________________________________________________________________
    private Feld[,] neues2dFeld()
    {
      Feld[,] neuesFeld = new Feld[mAnzahlSpalten, mAnzahlZeilen];


      for (int Spalte = 0; Spalte < mAnzahlSpalten; Spalte++)
      {
        for (int Zeile = 0; Zeile < mAnzahlZeilen; Zeile++)
        {
          neuesFeld[Spalte, Zeile] = new Feld();
        }
      }
      return neuesFeld;
    }
  }
}
