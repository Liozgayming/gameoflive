﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GameOfLive
{
  public class Feld
  {

    private int mGenerationenLebend;

    public bool Lebend
    {
      get;
      set;
    }
   
    public int GenerationenLebend
    {
      get
      {
        return mGenerationenLebend;
      }
      set
      {
        if (value < int.MaxValue)
        {
          mGenerationenLebend = value;
        }
      }
    }
  }
}